﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour 
{
	[SerializeField]
	private Text _scoreDisplay;

	public void SetScore(int score)
	{
		_scoreDisplay.text = score.ToString();
	}
}
