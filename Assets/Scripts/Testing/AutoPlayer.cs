﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Testing
{
	public class AutoPlayer : MonoBehaviour 
	{
		private TextObject[] _textObjects;


		IEnumerator Start () 
		{
			yield return null;
			_textObjects = FindObjectsOfType<TextObject>();

			while (true) 
			{
				yield return new WaitForSeconds(0.5f);
				for (int i = 0; i < _textObjects.Length; i++) 
				{
					if (_textObjects [i].Active) 
					{
						_textObjects[i].Click();
						break;
					}
				}
			}
		}
	}
}
