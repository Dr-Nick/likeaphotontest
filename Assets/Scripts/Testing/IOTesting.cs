﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace Testing
{
	public class IOTesting 
	{
		private static readonly string JSON_PATH = "myworld.ver1.words.json";


		private string _json;
		private bool _jsonLoadSuccess;
		private WordList _wordList;
		private bool _parseJSONSuccess;

		private void LoadJSONFile()
		{
			_json = string.Empty;
			_jsonLoadSuccess = TextFileLoader.LoadTextFileFromStreamingAssets(JSON_PATH, ref _json);
		}

		private void ParseJSONFile()
		{
			_wordList = null;
			_parseJSONSuccess = JsonParser.ParseJson<WordList> (_json, ref _wordList);
		}


		[Test]
		public void CanLoadJSON() 
		{
			LoadJSONFile();
			Assert.IsTrue(_jsonLoadSuccess);
		}

		[Test]
		public void CanParseJSON() 
		{
			LoadJSONFile();
			ParseJSONFile();
			Assert.IsTrue(_parseJSONSuccess);
		}

		[UnityTest]
		public IEnumerator HasAllAudio() 
		{
			LoadJSONFile();
			ParseJSONFile();

			int count = 0;
			int missing = 0;

			WordLookUp wl = new WordLookUp(_wordList);

			while (!wl.IsAudioSetUp) 
			{
				yield return null;
			}

			for (int i = 0; i < _wordList.payload.Length; i++) 
			{
				string path = _wordList.payload[i].audio.path;

				if (wl.GetAudioClip (path) != null) 
				{
					count++;	
				}
				else 
				{
					missing++;
				}
			}

			if (_wordList.payload.Length == 0) 
			{
				Assert.Fail();
			} 
			else 
			{
				Assert.AreEqual(count, count - missing);
			}
		}

		[Test]
		public void SaveScoreToPlayerPrefs()
		{
			ScoreDataPlayerPrefs.ClearScore();
			int score = 100;
			ScoreDataPlayerPrefs.SaveGameScore(score);
			Assert.Pass();
		}

		[Test]
		public void RetrieveScoreFromPlayerPrefs()
		{
			int score = 25;
			ScoreDataPlayerPrefs.SaveGameScore(score);
			int retrievedScore = ScoreDataPlayerPrefs.RetrieveGameScore();
			Assert.AreEqual(score, retrievedScore);
		}

	}
}