﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace Testing
{
	public class TextObjectTesting 
	{

		private TextObject _textObject;
		private int _gameScore = 0;




		private void GeneralSetup()
		{			
			GameObject canvas = new GameObject("Canvas");
			canvas.AddComponent<Canvas>();

			GameObject textObject = new GameObject("TextObject");
			textObject.transform.SetParent (canvas.transform);
			textObject.AddComponent<RectTransform>();
			_textObject = textObject.AddComponent<TextObject>();
		}

		private void AssignTextEvents()
		{
			_gameScore = 0;
			_textObject.OnClick += IncrementGameScore;
			_textObject.OnHide += OnHide;
			_textObject.OnReadyToSpawn += OnReadyToSpawn; 
		}

		private void ShowText()
		{
			var wi = new WordItem(){content = "Testing"};
			_textObject.ShowText(wi, new Vector3 (0, 0, 0), 1f);
		}

		private void IncrementGameScore(TextObject to)
		{
			_gameScore++;
		}

		private void OnHide(TextObject to)
		{
			
		}

		private void OnReadyToSpawn(TextObject to)
		{

		}



		[Test]
		public void TextFormattingRemoveLineBreaks() 
		{
			GeneralSetup();

			var wi = new WordItem(){content = "shell\n"};
			_textObject.ShowText(wi, new Vector3 (0, 0, 0), 1f);
			Assert.AreEqual(_textObject.DisplayText,"shell");
		}

		[Test]
		public void TextFormattingRemoveFullStop() 
		{
			GeneralSetup();
			var wi = new WordItem(){content = "favourite."};
			_textObject.ShowText(wi, new Vector3 (0, 0, 0), 1f);
			Assert.AreEqual(_textObject.DisplayText,"favourite");
		}

		[Test]
		public void TextFormattingRetainsApostrophe() 
		{
			GeneralSetup();
			var wi = new WordItem(){content = "Nick's"};
			_textObject.ShowText(wi, new Vector3 (0, 0, 0), 1f);
			Assert.AreEqual(_textObject.DisplayText,"Nick's");
		}

		[Test]
		public void TextFormattingRetainsApostrophe2() 
		{
			GeneralSetup();
			var wi = new WordItem(){content = "Nick\u2019s"};
			_textObject.ShowText(wi, new Vector3 (0, 0, 0), 1f);
			Assert.AreEqual(_textObject.DisplayText,"Nick\u2019s");
		}

		[Test]
		public void CheckThatTextNotVisibleByDefault() 
		{
			GeneralSetup();
			Assert.IsFalse(_textObject.Visible);
		}

		[Test]
		public void CheckThatTextNotActiveDefault() 
		{
			GeneralSetup();
			Assert.IsFalse(_textObject.Active);
		}

			
		[UnityTest]
		public IEnumerator TextObjectActiveAfterShowingText() 
		{
			GeneralSetup();
			ShowText();
			float countDown = 5;
			while (countDown > 0 && !_textObject.Active) 
			{
				countDown -= Time.deltaTime;
				yield return null;
			}
			Assert.True(_textObject.Active);
		}


		[UnityTest]
		public IEnumerator TextObjectHidesAfter5Seconds() 
		{
			GeneralSetup();
			var wi = new WordItem(){content = "5 Second Test"};
			_textObject.ShowText(wi, new Vector3 (0, 0, 0), 5f);
			float countDown = 2;
			while (countDown > 0 && !_textObject.Active) 
			{
				countDown -= Time.deltaTime;
				yield return null;
			}

			if (countDown < 0) 
			{
				//Assume the text never became active to begin with
				Assert.Fail ();
			} else 
			{
				yield return new WaitForSeconds(6f); //Allow a bit of leeway given it pops up first
				Assert.False(_textObject.Active);
			}
		}

		[Test]//Score shouldn't increment when text is inactive
		public void TextNotCallClickEventWhenInactive()
		{			
			GeneralSetup();
			AssignTextEvents();
			_textObject.Click();
			Assert.AreEqual(0, _gameScore);
		}

		[Test]//Score shouldn't increment when text is inactive
		public void TextNotCallClickEventWhenScalingUp()
		{			
			GeneralSetup();
			AssignTextEvents();
			ShowText();
			_textObject.Click();
			Assert.AreEqual(_gameScore, 0);
		}

		[UnityTest]
		public IEnumerator TextScoreIncreasesWhenClickedWhenActive()
		{			
			GeneralSetup();
			AssignTextEvents();
			ShowText();
			while (!_textObject.Active) 
			{
				yield return null;
			} 
			_textObject.Click();
			Assert.AreEqual(1,_gameScore);
		}
			
		[UnityTest]//Score shouldn't increment multiple times after it has been made active
		public IEnumerator TextCheckMultiClicksArentRegisteredOnceActive()
		{			
			GeneralSetup();
			AssignTextEvents();
			ShowText();
			while (!_textObject.Active) 
			{
				yield return null;
			} 
			_textObject.Click();
			_textObject.Click();
			Assert.AreEqual(1, _gameScore);
		}

	}
}
