﻿using System;

[Serializable]
public class WordList
{
	public WordItem[] payload;
}


[Serializable]
public class WordItem 
{
	public int id;
	public int view_order;
	public float duration;
	public string content;
	public AudioPath audio;
}

[Serializable]
public class AudioPath
{
	public string path;
}
