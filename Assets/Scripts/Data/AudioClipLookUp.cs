﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AudioClipLookUp  
{
	private Dictionary<string,AudioClip> _audioLookup = new Dictionary<string,AudioClip>();
	private Dictionary<string,UnityWebRequestAsyncOperation> _audioRequestLookup = new Dictionary<string,UnityWebRequestAsyncOperation>();

	public bool IsLoadingContent
	{
		get
		{
			return _audioRequestLookup.Count > 0;
		}
	}


	public void LoadAudioClip(string path)
	{
		string fullPath = AudioLoading.GenerateAudioFilePath(path);
		if (_audioLookup.ContainsKey(fullPath)) 
		{
			return;
		}

		var routine = AudioLoading.LoadAudio(fullPath,AddAudioClip);
		if (!routine.isDone) 
		{
			_audioRequestLookup.Add(fullPath, routine);
		}
	}


	public AudioClip GetAudioClip(string path)
	{
		string fullPath = AudioLoading.GenerateAudioFilePath(path);

		if (_audioLookup.ContainsKey(fullPath)) 
		{
			return (_audioLookup[fullPath]);
		} 
		return null;
	}

	private void AddAudioClip(AsyncOperation ao)
	{
		UnityWebRequestAsyncOperation wr = ao as UnityWebRequestAsyncOperation;

		if (wr.webRequest.isNetworkError) 
		{
			Debug.Log (wr.webRequest.url + " " + wr.webRequest.error);
		} 
		else if (wr.webRequest.isHttpError) 
		{
			Debug.Log (wr.webRequest.url + " " + wr.webRequest.error);
		} 
		else 
		{
			var audioClip = DownloadHandlerAudioClip.GetContent(wr.webRequest);
			_audioLookup.Add (wr.webRequest.url, audioClip);
		}
		_audioRequestLookup.Remove(wr.webRequest.url);
	}	

}
