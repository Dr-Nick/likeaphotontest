﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TextFileLoader : MonoBehaviour 
{
	public static bool LoadTextFileFromStreamingAssets(string relativePath, ref string output)
	{		
		var path = Path.Combine(Application.streamingAssetsPath,relativePath);
		try
		{
			output = File.ReadAllText(path);
			return true;
		}
		catch
		{
		}
		return false;
	}		
}
