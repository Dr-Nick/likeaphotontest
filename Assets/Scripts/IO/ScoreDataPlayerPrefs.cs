﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class ScoreDataPlayerPrefs
{
	private static readonly string SCORE = "GAME_SCORE";


	public static int RetrieveGameScore()
	{
		return PlayerPrefs.GetInt (SCORE, 0);
	}

	public static void SaveGameScore(int value)
	{
		PlayerPrefs.SetInt(SCORE, value);
	}

	#if UNITY_EDITOR
	[MenuItem("Testing/Clear Player Prefs")]
	#endif
	public static void ClearScore()
	{
		PlayerPrefs.DeleteAll ();
	}
}
