﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonParser 
{
	public static bool ParseJson<T>(string json, ref T output)
	{
		try 
		{
			output = JsonUtility.FromJson<T>(json);
			return true;
		}catch
		{
			return false;
		}
	}
}
