﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

public class AudioLoading 
{
	public static string GenerateAudioFilePath(string relativePath)
	{
		var pathBuilder = new StringBuilder ();
		pathBuilder.Append("file://");
		pathBuilder.Append(Application.streamingAssetsPath);
		pathBuilder.Append("\\/");
		pathBuilder.Append (relativePath);


		#if UNITY_STANDALONE || UNITY_EDITOR
		pathBuilder.Replace(".mp3",".ogg");
		#endif
		return pathBuilder.ToString();
	}

	public static UnityWebRequestAsyncOperation LoadAudio(string path, System.Action<AsyncOperation> onLoad)
	{
		#if UNITY_STANDALONE || UNITY_EDITOR
		var audioType = AudioType.OGGVORBIS; 
		#else
		var audioType = AudioType.AudioType.MPEG;
		#endif


		var audioDownloader = UnityWebRequestMultimedia.GetAudioClip(path,audioType);
		UnityWebRequestAsyncOperation wr = audioDownloader.SendWebRequest();
		wr.completed += onLoad;

		return wr;
	}

}
