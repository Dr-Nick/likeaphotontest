﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour 
{
	private const int AUDIO_SOURCE_COUNT = 4;

	private AudioSource[] _audioSourceList;
	private int _audioIndex = 0;



	// Use this for initialization
	void Awake () 
	{
		_audioSourceList = new AudioSource[AUDIO_SOURCE_COUNT];
		for (int i = 0; i < AUDIO_SOURCE_COUNT; i++) 
		{
			_audioSourceList[i] = this.gameObject.AddComponent<AudioSource>();
			_audioSourceList[i].playOnAwake = false;
		}
	}

	public void PlayAudioClip(AudioClip audio)
	{
		_audioSourceList[_audioIndex].clip = audio;
		_audioSourceList[_audioIndex].Play();
		_audioIndex = (_audioIndex + 1) % AUDIO_SOURCE_COUNT;
	}


}
