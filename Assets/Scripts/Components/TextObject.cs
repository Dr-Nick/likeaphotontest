﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[RequireComponent(typeof(Text),typeof(Canvas)),DisallowMultipleComponent]
public class TextObject : MonoBehaviour 
{
	[SerializeField]
	private AnimationCurve _scaleUpCurve = AnimationCurve.EaseInOut(0,0,1f,1f); 
	[SerializeField]
	private AnimationCurve _scaleDownCurve = AnimationCurve.EaseInOut(0,1f,1f,0f);
	[SerializeField]
	private float _scaleDuration = 1.0f;

	private Text _text;
	private Canvas _canvas;
	private Transform _transform;
	private bool _active = false;
	private float _activeDuration;
	private Action<TextObject> _onClick;
	private Action<TextObject> _onHide;
	private Action<TextObject> _onReadyToSpawn;
	private WordItem _currentWordItem;


	public event Action<TextObject> OnClick
	{
		add 	{_onClick += value;}
		remove 	{ _onClick -= value;}
	}

	public event Action<TextObject> OnHide
	{
		add 	{_onHide += value;}
		remove 	{ _onHide -= value;}
	}

	public event Action<TextObject> OnReadyToSpawn
	{
		add 	{_onReadyToSpawn += value;}
		remove 	{ _onReadyToSpawn -= value;}
	}

	public string WordAudioPath
	{
		get{ return _currentWordItem.audio.path; }
	}

	public bool Visible
	{
		get {return _canvas.enabled;}
		private set {_canvas.enabled = value;}
	}

	public string DisplayText
	{
		get {return _text.text;}
		private set 
		{
			//Remove any non alpha numeric characters
			char[] arr = value.Where(c => (char.IsLetterOrDigit(c) ||
				c == ' ' || c == '\'' || c == '\u2019' ||
				c == '-')).ToArray(); 

			_text.text = new string(arr);
		}
	}

	public bool Active
	{
		get { return _active; }
	}

	public void ShowText(WordItem wi, Vector3 position, float activeDuration)
	{
		SetScale(0);
		Visible = true;
		DisplayText = wi.content;
		_transform.localPosition = position;
		_activeDuration = activeDuration;
		_currentWordItem = wi;
		ScaleUp();
	}
		
	public void Click()
	{
		if (!_active) 
		{
			return;
		}
		if (_onClick != null) 
		{
			_onClick(this);
		}
		ScaleDown();
	}

	public void ForceWaitToSpawn()
	{
		if (!_active) 
		{
			StopAllCoroutines();
			if (Visible) 
			{
				Visible = false;
				if (_onHide != null) 
				{
					_onHide(this);
				}
			}
			StartCoroutine(WaitToSpawn());
		}
	}

	void Awake()
	{
		_text = GetComponent<Text>();
		_canvas = GetComponent<Canvas>();
		_transform = this.transform;
		Visible = false;
	}


	private void SetScale(float scale)
	{
		_transform.localScale = new Vector3 (scale, scale, scale);
	}

	private void ScaleUp()
	{
		if (!_active) 
		{
			StartCoroutine (ScaleRoutine (_scaleUpCurve, _scaleDuration, OnScaleUp));
		}
	}

	private void ScaleDown()
	{
		if (_active) 
		{
			_active = false;
			StartCoroutine(ScaleRoutine(_scaleDownCurve, _scaleDuration, OnScaleDownComplete));
		}
	}
		
	private void OnScaleUp()
	{
		StartCoroutine(WaitDuration ());
	}

	private void OnScaleDownComplete()
	{
		ForceWaitToSpawn();
	}

	private IEnumerator WaitToSpawn()
	{
		yield return new WaitForSeconds(2f);
		if (_onReadyToSpawn != null) 
		{
			_onReadyToSpawn(this);
		}
	}


	private IEnumerator WaitDuration()
	{
		_active = true;
		while (_active) 
		{
			_activeDuration -= Time.deltaTime;
			if (_activeDuration < 0) 
			{
				break;
			}
			yield return null;
		}
		ScaleDown();
	}

	private IEnumerator ScaleRoutine(AnimationCurve curve, float duration, Action onComplete)
	{
		float t = 0;
		float timeElapsed = 0;
		float val = 0;
		Vector3 _currentScale = new Vector3 (0, 0, 0);

		while (timeElapsed < duration) 
		{
			timeElapsed += Time.deltaTime;
			t = timeElapsed / duration;
			val = curve.Evaluate(t);
			_currentScale.Set(val, val, val);
			_transform.localScale = _currentScale;
			yield return null;
		}

		val = curve.Evaluate(t);
		_currentScale.Set(val, val, val);
		_transform.localScale = _currentScale;
		if (onComplete != null) 
		{
			onComplete ();
		}
	}

}
