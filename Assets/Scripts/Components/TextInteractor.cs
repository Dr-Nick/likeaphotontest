﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(TextObject)),DisallowMultipleComponent]
public class TextInteractor : MonoBehaviour, IPointerClickHandler 
{
	private TextObject _textObject;


	public void OnPointerClick (PointerEventData eventData)
	{
		_textObject.Click();
	}


	void Awake()
	{
		_textObject = GetComponent<TextObject>();
	}
}
