﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordLookUp 
{

	private AudioClipLookUp _audioLookup;
	private WordList _audioList;
	private int _index; //The current index into _wordIndexLookUp
	private int[] _wordIndexLookUp; //Contains a list of indices into the WordList. Allows for randomisation

	public WordLookUp(WordList al)
	{
		SetUp(al);
		PreloadAllAudio();
	}

	public WordLookUp(WordList al, bool preloadAudio)
	{
		SetUp(al);
		if (preloadAudio) 
		{
			PreloadAllAudio();
		}
	}


	public bool IsAudioSetUp
	{
		get 
		{
			if (_audioLookup != null) 
			{
				return !_audioLookup.IsLoadingContent;
			}
			return false;
		}
	}


	public AudioClip GetAudioClip(string path)
	{
		if (IsAudioSetUp) 
		{
			return _audioLookup.GetAudioClip(path);
		}
		throw new System.Exception("WordLookUp Audio not setup");
	}


	public WordItem GetNextWordItem()
	{
		var item = _audioList.payload[_wordIndexLookUp [_index]];
		_index = (_index + 1) % _wordIndexLookUp.Length;
		if (_index == 0) 
		{
			RandomiseList();
		}
		return item;
	}

	public void RandomiseList()
	{
		for (int i = 0; i < _wordIndexLookUp.Length; i++) 
		{
			int randomIndex = Random.Range(0, _wordIndexLookUp.Length);
			int indexA = _wordIndexLookUp [i];
			int indexB = _wordIndexLookUp [randomIndex];
			_wordIndexLookUp [i] = indexB;
			_wordIndexLookUp [randomIndex] = indexA;
		}
	}

	private void SetUp(WordList al)
	{
		_audioList = al;
		_wordIndexLookUp = new int[_audioList.payload.Length];

		for (int i = 0; i < _wordIndexLookUp.Length; i++) 
		{
			_wordIndexLookUp[i] = i;
		}
	}

	private void PreloadAllAudio()
	{
		_audioLookup = new AudioClipLookUp();
		for (int i = 0; i < _audioList.payload.Length; i++) 
		{
			_audioLookup.LoadAudioClip(_audioList.payload[i].audio.path);
		}
	}

}
