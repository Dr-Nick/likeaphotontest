﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour 
{
	public const int MAX_TEXT_OBJECTS = 5;
	public const float MAX_VISIBLE_TIME = 5f;

	[SerializeField,Tooltip("The prefab for the text object")]
	private TextObject _textObjectPrefab;

	[SerializeField,Tooltip("The root transform that the TextObjects are spawned under")]
	private Transform _rootTransform;

	[SerializeField,Tooltip("The positions where the text spawns")]
	private Transform[] _spawnPositions;

	[SerializeField,Tooltip("The path that json is loaded from")]
	private string _jsonPath;

	[SerializeField,Tooltip("The audio player thats play word audio")]
	private AudioPlayer _audioPlayer;

	[SerializeField, Tooltip("Displays the game's ui")]
	private GameUI _gameUI;


	private TextObject[] _textObjectList; 
	private Dictionary<TextObject,Transform> _spawnPointLookup;
	private WordLookUp _wordLookup;
	private int _score = 0;
	private Action<int> _onUpdateScore;


	public int Score
	{
		get { return _score; }
		set 
		{ 
			_score = value;
			if (_onUpdateScore != null) 
			{
				_onUpdateScore(_score);
			}		
		}
	}

	public void IncrementScore(TextObject to)
	{
		var audioPath = to.WordAudioPath;
		_audioPlayer.PlayAudioClip(_wordLookup.GetAudioClip(audioPath));
		Score++;
	}

	public void RemoveTextObject(TextObject to)
	{
		_spawnPointLookup[to] = null;
	}



	public void SpawnTextObject(TextObject to)
	{
		Transform spawnPoint = null;

		int startIndex = UnityEngine.Random.Range(0, _spawnPositions.Length);
		int index = 0;

		for (int i = 0; i < _spawnPositions.Length; i++) 
		{
			index = (startIndex + i) % _spawnPositions.Length; 

			if (!_spawnPointLookup.ContainsValue(_spawnPositions[index])) 
			{
				spawnPoint = _spawnPositions[index];
				break;
			}
		}

		if (spawnPoint != null) 
		{
			_spawnPointLookup[to] = spawnPoint;
			var wordItem = _wordLookup.GetNextWordItem();
			to.ShowText(wordItem,spawnPoint.localPosition,MAX_VISIBLE_TIME);
		} 
		else 
		{
			//No available spawn points - Need to wait again
			to.ForceWaitToSpawn();
		}
	}




	// Use this for initialization
	void Start () 
	{
		if (_gameUI != null) 
		{
			_onUpdateScore += _gameUI.SetScore;
		}
		Score = ScoreDataPlayerPrefs.RetrieveGameScore();

		string json = string.Empty;
		if (!TextFileLoader.LoadTextFileFromStreamingAssets (_jsonPath, ref json)) 
		{
			Debug.LogError("Cannot read from text file " + _jsonPath);
			return;
		}

		WordList worldList = null;

		if (!JsonParser.ParseJson<WordList> (json, ref worldList)) 
		{
			Debug.LogError("Cannot parse json");
			return;
		};

		if (worldList.payload.Length == 0) 
		{
			Debug.LogError("No content in wordList");
			return;
		}
			
		_textObjectList = new TextObject[MAX_TEXT_OBJECTS];

		_spawnPointLookup = new Dictionary<TextObject,Transform>(MAX_TEXT_OBJECTS);

		for (int i = 0; i < MAX_TEXT_OBJECTS; i++) 
		{
			_textObjectList[i] = Instantiate<TextObject>(_textObjectPrefab, _rootTransform);
			_textObjectList[i].OnClick += IncrementScore;
			_textObjectList[i].OnHide += RemoveTextObject;
			_textObjectList [i].OnReadyToSpawn += SpawnTextObject;
			_spawnPointLookup.Add(_textObjectList[i], null);
		}

		_wordLookup = new WordLookUp(worldList);
		StartCoroutine(GameRoutine());
	}

	void OnDestroy()
	{
		ScoreDataPlayerPrefs.SaveGameScore(Score);
		if (_gameUI != null) 
		{
			_onUpdateScore -= _gameUI.SetScore;
		}
	}

	private IEnumerator GameRoutine()
	{
		while (!_wordLookup.IsAudioSetUp) 
		{
			yield return null;
		}
		_wordLookup.RandomiseList();


		for (int i = 0; i < MAX_TEXT_OBJECTS; i++) 
		{
			SpawnTextObject(_textObjectList [i]);
			yield return new WaitForSeconds(0.3f);
		}
	}
				
}
